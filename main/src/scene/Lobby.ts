class Lobby extends BaseScene implements eui.UIComponent {
	public back: eui.Button;
	public lobbyLayout: eui.Group;
	public roomListLayout: eui.Group;
	public roomListView: eui.List;
	public roomListTitle: eui.Label;
	public create: eui.Button;
	public join: eui.Button;
	public numpad: eui.Group;
	public b1: eui.Button;
	public b2: eui.Button;
	public b3: eui.Button;
	public b4: eui.Button;
	public b5: eui.Button;
	public b6: eui.Button;
	public b7: eui.Button;
	public b8: eui.Button;
	public b9: eui.Button;
	public b10: eui.Button;
	public b11: eui.Button;
	public b12: eui.Button;
	public roomIDInput: eui.TextInput;
	public roomLayut: eui.Group;
	public createRoom: eui.Button;
	public joinRoom: eui.Button;
	public bg: eui.Image;



	public name: string;

	public constructor() {
		super();
		console.log('[INFO] login');

	}
	protected onShow(par) {
		console.log("[Lobby] onShow:" + par);
	}
	protected onCreated(): void {
		console.log("[Lobby] [onCreated] " + this.name);
		ShaderUtils.filter(this.join);
		ShaderUtils.shader(this.lobbyLayout, ShaderUtils.CustomFilter.customFilter3);


		var bg: egret.Bitmap = new egret.Bitmap();
		bg.texture = RES.getRes("firePot_png");
		this.addChild(bg);
		bg.x = App.W / 2 - bg.width / 2 - 50;
		bg.y = App.H / 2 - bg.height / 2 - 210;
		bg.scaleX = bg.scaleY = 1.8;

		var system = new particle.GravityParticleSystem(RES.getRes("ballParticle_png"), RES.getRes("ballParticle_json"));
		this.addChild(system);
		system.start();
		system.y = App.H / 2;
		system.x = App.W / 2;
		system.emitterX = 0;
		system.emitterY = 0;
		system.scaleX = system.scaleY = 1.5;
	}

	public onClick(name: string, v: egret.DisplayObject) {
		super.onClick(name, v);
		switch (name) {
			case "b1":
			case "b2":
			case "b3":
			case "b4":
			case "b5":
			case "b6":
			case "b7":
			case "b8":
			case "b9":
			case "b10":
				this.roomIDInput.text += (<eui.Button>v).label;
				break;
			case "b12":
				this.numpad.visible = !this.numpad.visible;
				break;
			case "b11":
				this.roomIDInput.text = "";
				break;
			case "join":
				this.numpad.visible = !this.numpad.visible;
				break;
			case "create":
				Toast.show(name);
				break;
			default:
				Toast.show(name);

				break;
		}
	}
	protected onDestory(): void {
		console.log("[Lobby] [onDestory] " + this.name);
	}
}