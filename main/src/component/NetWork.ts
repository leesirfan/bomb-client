class NetWork {
	
	public constructor() {
	}
	public static send(msgType: string, msg: any) {
		//TODO 更换网络模块
		try {
			UmspManager.send(msgType, msg);

		} catch (error) {
			console.log('[ERROR]  ' + error);
		}
	}
	private static receiveMap = {};
	public static receive(msgType: string, callBack: Function) {
		NetWork.receiveMap[msgType] = callBack;
	}

	public static onMsg(data) {
		var json = JSON.parse(data);
		console.log('[INFO] ',json["type"],' data:', data);
		NetWork.receiveMap[json["type"]] && NetWork.receiveMap[json["type"]](json["data"]);
	}

	public static connect(roomUserChangedListener: Function) {
		var loginRsp = function (rsp) {
			UI.printLog("[Rsp]login.status:" + utf8ByteArrayToString(rsp.payload));
			this.match(NetWork.onMsg, roomUserChangedListener);
		}.bind(this);
		var that = this;
		var linstener = {
			onConnect: function (host) {
				UI.printLog("[NetWork] [Connect success]: host:" + host + "] ");

			}.bind(that),
			onErr: function (errCode, errMsg) {
				UI.printLog("[NetWork] [onErr]:[" + errCode + "] errMsg:" + errMsg);
				Toast.show("NetWork Err :" + errCode);
			}.bind(that),
			onDisConnect: function (errCode, errMsg, host) {
				UI.printLog("[NetWork] [disConnect] host:" + host + ", errCode:" + errCode + " errMsg:" + errMsg);
			}.bind(that)
		};
		UmspManager.login(loginRsp, linstener);
	}

	private static match(roomMsgListener, roomUserChangedListener) {
		var rsp = function (rsp) {
			var json = JSON.parse(utf8ByteArrayToString(rsp.payload));
			if (json["isSuccess"] === true) {
				var room = json["room"];
				UI.printLog("=========================================================");
				UI.printLog("[Rsp]matchRoomID:" + room["roomID"]);
				UI.printLog("=========================================================");
				UI.printLog("                                                         ");
			} else {
				UI.printLog("[W] match fail");
			}
		}.bind(this);
		var ui = this;
		var connectListener = {
			onConnect: function (host) {
				UI.printLog("[NetWork] [Connect success]: host:" + host + "] ");
				// this.netStateBar.state(NetStateBar.Connect);
			}.bind(ui),
			onErr: function (errCode, errMsg) {
				UI.printLog("[NetWork] [onErr]:[" + errCode + "] errMsg:" + errMsg);
				Toast.show("网络异常,请返回重试");
			}.bind(ui),
			onDisConnect: function (errCode, errMsg, host) {
				UI.printLog("[NetWork] [disConnect] host:" + host + ", errCode:" + errCode + " errMsg:" + errMsg);
			}.bind(ui)
		};
		// var roomUserChangedListener = function (rsp) {
		// 	var userChanged = JSON.parse(utf8ByteArrayToString(rsp.payload));
		// 	UI.printLog("[Rsp]room userID:" + userChanged["userID"] + " changed :" + userChanged["userAction"]);
		// 	var currentUserList = userChanged["currentUserList"];
		// 	var newList = [currentUserList.length];
		// 	for (var i = 0; i < currentUserList.length; i++) {
		// 		UI.printLog("[Rsp]room UserList[" + i + "]:" + currentUserList[i]["userID"]);
		// 		newList[i] = currentUserList[i]["userID"];
		// 	}
		// 	ListViewUtil.refreshData(this.roomListView, newList);
		// 	if (userChanged["userAction"] == "enter") {
		// 		if (this.isDrawer) {
		// 			this.requestIAmDrawer();
		// 			this.syncWord();
		// 			this.isHasDraw = true;
		// 		}
		// 		Toast.show("玩家 " + userChanged["userID"] + " 进入房间");
		// 	} else if (userChanged["userAction"] == "exit") {
		// 		Toast.show("玩家 " + userChanged["userID"] + " 离开房间");
		// 	}

		// 	this.userList = currentUserList;
		// }.bind(this);
		// var roomMsgListener = function (data) {
		// 	// UI.printLog("[Rsp]room data:" + data);
		// 	this.onMsg(data);
		// }.bind(this);
		UmspManager.match(rsp, roomUserChangedListener, connectListener, roomMsgListener);
	}
}