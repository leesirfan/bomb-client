/*
Title: 泡泡堂
Sort: 3
*/

--------------------

​    `泡泡堂`是一款 `实时` , `在线`,`休闲`,`对抗`联网游戏. 起源与FC游戏 `炸弹人`

​    最大人数支持4V4,可以算得上中型网络游戏.对联网实时性要求较高, 复刻此款游戏,采用CS架构是比较好的选择.

> 游戏体验地址
- http://118.24.53.22:11111/BombBoy/ 默认2人开局
- http://118.24.53.22:11111/BombBoy/?maxuser=8 可选8人开局

> 完整源代码
- https://gitee.com/geliang/bomb-server/
- https://gitee.com/geliang/bomb-client/

![GIF](http://fluttergo.com/Game/BombBoy.assets/GIF.gif)


## 开发工具:

- 游戏引擎 Egret 5.2.13 + Android SDK 16
- 开发语言 Java&JavaScript
- 运行环境 Chrome&Android
- 辅助软件 Tiled/Dragonbones/Egret Feature
- 服务端框架 Umsp

## 游戏设计

### 游戏场景

习惯在开始前设置一个检查场景
假设在地图(4,1)处放置一个威力为9格的炸弹, 我们预期连锁引爆了位于(0,1)处的炸弹
![1545374138085](http://fluttergo.com/Game/BombBoy.assets/1545374138085.png)

####  1.1 地图节点类型

 不同类型代表地图上不同性质的物体
```java
    public final static int NULL = 0;//空
    public final static int DESTROY = 1;//可摧毁的物体
    public final static int OBSTACLES = 2;//不可摧毁的物体
    public final static int Bomb = 3;//炸弹
    public final static int ITEM_INDEX = 4;//道具起点索引
    public final static int ITEM_SPEED = 5;//道具:速度
    public final static int ITEM_COUNT = 6;//道具:增加数量
    public final static int ITEM_POWER = 7;//道具:增加威力

```


#### 1.2 地图结构
用一个二维数组来描述地图上xy方向上各坐标上的节点
```java
public class Map {
    public static int[][] mapArray = new int[][]{
            {1, 0, 0, 1, 1, 1, 0, 0},
            {3, 0, 0, 0, 0, 2, 1, 0},
            {2, 2, 1, 1, 1, 1, 1, 1},
            {1, 1, 2, 1, 0, 2, 1, 1},
            {0, 1, 2, 1, 1, 2, 1, 1},
            {1, 1, 2, 1, 0, 2, 1, 1},
            {0, 1, 1, 1, 1, 1, 1, 0},
            {0, 0, 1, 1, 0, 1, 0, 0},
    };
}
```

游戏核心逻辑就是对这个数组的操作. 
具体逻辑实现见完整代码: https://gitee.com/geliang/bomb-server/blob/master/src/BombController.java

## 游戏网络同步

游戏同步方面采用`帧同步`+`状态同步`混合的方式:

- `帧同步` 同步玩家操作(帧同步的帧率暂定为10)
- `状态同步` 同步地图(即上面的二维数组)状态

为了防止作弊, 帧同步的计算在服务端

由客户端只上传自己的输入状态枚举值(8方向+没动). 服务端按固定帧率根据每个玩家的当前输入状态推演下一帧玩家的 `x,y`,`物体碰撞`,`道具拾取`及`地图状态`,再广播同步给房间内所有玩家. 

## 服务端与客户端开发

#### 服务端 使用`umsp`二次开发完成玩家匹配,游戏逻辑计算

`umsp`是基于netty写的一个分布式网络开发框架 ,特点是

- 性能不错, 可动态扩容, 理论上只要机器够,支持海量的玩家同时在线进行频繁的数据交互,如MOBA游戏
- 可同时支持 `私有tcp`/`websocket`/`kcp`协议. 适用于H5/Native游戏二次开发
- 玩家匹配服务独立,  匹配规则自定义,甚至可将客户端链接调度到localhost的机器上,方便调试.
- 编译部署方便,  配合idea一键打包, 4个jar包走天下(4个服务可分开部署)

  


#### 客户端 使用`umsp`完成客户端通信功能, 使用`egret` 完成渲染部分的功能

`umsp`在客户端有 `JavaScript`/`c/c++`/`Java`各自实现了基础通信库 ,适配了H5/Android/iOS/Windows平台 ,另外用在c库的基础上封装了一层c#可集成至Unity.

![1550821505025](http://fluttergo.com/Game/BombBoy.assets/1550821505025.png)

> umsp框架源代码: https://gitee.com/geliang/Umsp-netty

## 游戏渲染

客户端渲染主要是人物行走帧动画, 地图的瓦片贴图和炸弹爆炸的帧动画.

egret的Dragonbones+Egret Feature解决帧动画

Tiled解决瓦片贴图的设计,瓦片贴图渲染遇到麻烦:egret 自带TileMap库不够完善. 

故用TypeScript重写了一套

> Tilemap库: https://gitee.com/geliang/bomb-client/tree/features-server/main/src/tilemap
![1550822509817](http://fluttergo.com/Game/BombBoy.assets/1550822509817.png)

服务端为了方便调试.用java的 swing 做一个地图+玩家数据变化的可视化

> swing: https://gitee.com/geliang/bomb-server/blob/master/src/Window.java

## 其他

webscoket 毕竟基于Tcp. 在手机4G/Wifi上玩`泡泡堂`难免卡顿. 使用kcp优化网络传输后效果有极大改善玩家操控体验.

有关kcp的介绍见:

> <kcp快在哪>: [../Network/kcp](http://fluttergo.com/Game/../Network/kcp) 

有兴趣的可以体验一下kcp加持的Android版本泡泡堂:

> Android版本的泡泡堂下载: http://118.24.53.22:11111/BombBoyAndroid.apk

